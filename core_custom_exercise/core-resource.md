## Extend a Core Resource

In this exercise, you will create core Flow `customers` to extend the Customer resource with a Field wishlists that will allow it to be connected to the custom resource `wishlists` from the previous exercise.

### Create Customers Flow (this extends the customers endpoint with the addition fields)

Complete the following steps to extend the customers entity:

* In Postman, Open the `Create a flow` request.
* The `POST` request URL is `{{baseUrl}}/flows/`
* Replace the contents in the `Body` section with:

```json
{
  "data": {
    "type": "flow",
    "name": "Customers",
    "slug": "customers",
    "description": "Extends the default customer object",
    "enabled": true
  }
}
```
* Replace the contents in the `Tests` section with:
```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const id = Array.isArray(d) ? d[0].id : d.id
const slug = Array.isArray(d) ? d[0].slug : d.slug
pm.environment.set("customersFlowID", id)
pm.environment.set("customersFlowSlug", slug)
```

* Click `Send`
> This step creates the container that enables the customer endpoint to be extended and adds environment variables for customer flow ID and customer flow slug for use in later steps.

### Add Fields to the Customers Flow

Now, create a new Field of relationship type to link a customer to wishlists entries:

* In Postman, open the `Create a field` request.
* The `POST` request URL is `{{baseUrl}}/fields`
* Replace the contents in the `body` section with:

```json
{
    "data": {
        "type": "field",
        "name": "Wishlists",
        "slug": "wishlists",
        "field_type": "relationship",
        "validation_rules": [{
            "type": "one-to-many",
            "to": "wishlists"
        }],
        "description": "Customers wishlists",
        "unique": false,
        "enabled": true,
        "required": false,
        "relationships": {
            "flow": {
                "data": {
                    "type": "flow",
                    "id": "{{customersFlowID}}"
                }
            }
        }
    }
}
```

* Replace the contents in the `Tests` section with:

```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const id = Array.isArray(d) ? d[0].id : d.id
const slug = Array.isArray(d) ? d[0].slug : d.slug
pm.environment.set("wishlistFieldID", id)
pm.environment.set("wishlistFieldSlug", slug)
```

* Click `Send`

### Create a New Customer

* In Postman, open the `Create a customer` request from `customers` folder.
*
* Replace the data in the `Body` section with::
```json
{
  "data": {
    "type": "customer",
    "name": "Ron Swanson",
    "email": "ron@example.com",
    "password": "mysecretpassword"
  }
}
```
* Replace the contents in the `Tests` section with:
```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const customerID = Array.isArray(d) ? d[0].id : d.id
pm.environment.set("customerID", customerID)
```
* Click `Send` to create a new customer Entry.

[Next: Link a customer to a wishlist](./core-flow-entries.md)