## Create a Custom Resource

In this exercise, you will create a custom Flow `Wishlists` with a relationship field for `products` that stores the customer's desired products.

### Steps:
* Create the flow - this creates a new endpoint you can add entries to.
* Add fields to the flow endpoint - You can define up to 100 fields to a flow. Of these fields, we will be creating a `relationship` field type. This is a special type of field can can relate objects to each other. Relationship fields can be added only after the entry has been created. So, we will create the entry before we add the relationship.
### Create a New Custom Flow

Complete the following steps to create a custom Flow:

* the `POST` request URL is `{{baseUrl}}/flows/`
* In Postman, open the `Create a flow` request from `flows` folder.
* Replace the contents in the `Body` section with:

```json
{
    "data": {
    "type": "flow",
    "name": "Wishlists",
    "slug": "wishlists",
    "description": "Allow customers to store products they want to purchase at a later date",
    "enabled": true
    }
}
```
```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const flowID = Array.isArray(d) ? d[0].id : d.id
pm.environment.set("flowID", flowID)
const flowSlug = Array.isArray(d) ? d[0].slug : d.slug
pm.environment.set("flowID", flowSlug)
```

* Click `Send`.
### PostHoc
> The postman Tests script has stored the `flow ID` in an environemnt ID in a variable called `flowID` - this varialbe will be used in the request to add the field relationship to the flow.
> The postman Tests script has stored the `flow slug` in an environemnt ID in a variable called `flowSlug` - this will be used with creating entries in the flow.
### Add a Field to the Custom Flow

In this step, you will create a Field `products` to store the customers' desired products.

* In Postman, open the `Create a field` request from `flows` folder.
* The `POST` request URL is `{{baseUrl}}/fields/`
* Replace the contents in the body section with::

```json
{
    "data": {
        "type": "field",
        "name": "Products",
        "slug": "products",
        "field_type": "relationship",
        "validation_rules": [{

            "type": "one-to-many",
            "to": "products"
        }],
        "description": "Wishlist Products",
        "required": false,
        "enabled": true,
        "relationships": {
            "flow": {
                "data": {
                    "type": "flow",
                    "id": "{{flowID}}"
                }
            }
        }
    }
}
```

```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const fieldID = Array.isArray(d) ? d[0].id : d.id
const fieldSlug = Array.isArray(d) ? d[0].slug : d.slug
pm.environment.set("fieldID", fieldID)
pm.environment.set("fieldSlug", fieldSlug)
```
* Click `Send`.
> The script in the Tests section of the request stores the Field ID returned in a variable called `fieldID`.
> The script in the Tests section of the request stores Field Slug in your environment called `fieldSlug`.

### Get the Wishlist Fields

You've created a custom resource with a relationship Field to store the product IDs. Next, send a GET request to get the resource and all fields on it.

* In Postman, open the `Get all fields on a flow` request from `flows` folder.
* The `POST` request URL is `{{baseUrl}}/flows/{{flowSlug}}/fleids`
* Select `Send`.
* The response should look something like this.
```json
{
    "data": [
        {
            "id": "33ccecbb-bc97-4586-b05a-928d8a573125",
            "type": "field",
            "field_type": "relationship",
            "slug": "products",
            "name": "Products",
            "description": "Wishlist Products",
            "required": false,
            "default": null,
            "enabled": true,
            "order": null,
            "omit_null": false,
            "validation_rules": [
                {
                    "type": "one-to-many",
                    "to": "products"
                }
            ],
            "links": {
                "self": "https://api.moltin.com/v2/flows/badbbd9e-bf5c-4db2-a2ed-15f96a37736d/fields/33ccecbb-bc97-4586-b05a-928d8a573125"
            },
            "relationships": {
                "flow": {
                    "data": {
                        "id": "badbbd9e-bf5c-4db2-a2ed-15f96a37736d",
                        "type": "flow"
                    }
                }
            },
            "meta": {
                "timestamps": {
                    "created_at": "2022-01-06T18:25:16.233Z",
                    "updated_at": "2022-01-06T18:25:16.233Z"
                }
            }
        }
    ]
}
```

[Next: Create a wishlist](./custom-flow-entries.md)