## Create Flows

There are two sets of exercises to complete that will lead you through creating and relating core, custom, and template flows. They can be completed in either order.

In the [Core & Custom Flows exercise](core_custom_exercise/instructions.md), you will use flows to extend the data schema to store wishlists and extend the customer resource to associate it with the wishlist resource.

In the [Template Flows exercise](template_exercise/instructions.md), you will use flows to create additional attributes for gaming products.  

### Prerequisites

You need to have the following tools installed:

* Postman (free version)

### Load API collection in Postman

Before you proceed to the exercises, get your API collection ready in the Postman:

* Download the free version of the Postman API tool.
* Download the Elastic Path Commerce Cloud Postman [Collection](postman/collection_20211002.json) and [Environment](postman/environment_20211002.json) files.
* Import the downloaded collection and environment into Postman.

>These instructions were developed for the `EPCC - Oct 2, 2021` collection version. This collection has been provided at the links above. You can find the most recently released Elastic Path Commerce Cloud Postman Collection [here](https://documentation.elasticpath.com/commerce-cloud/docs/developer/how-to/test-with-postman-collection.html). 

### Set up the Training Environment

You will be using the API key from your training store to create a new environment in Postman.
>You can request a training store by following the instructions from the [Request a Training Store](https://learn.elasticpath.com/learn/course/internal/view/elearning/11/epcc103-requesting-an-elastic-path-commerce-cloud-training-store) module.

* Visit https://dashboard.elasticpath.com/ and make note of your `Client ID` and `Client secret`.
* In Postman, complete the following steps:

    1. Select the `EPCC Environment` environment from the upper-right hand corner.
    2. Update environment variables named `clientID` and `clientSecret` using the values from the previous step.

As you progress through the collection, additional variables will be created and used from request to request. You can confirm the value of any variable's current value from the environment settings.

>Variables in Postman are in double curly braces `{{}}`.

### Authenticate

* Open the `Client credential token` request under `Authentication` folder.
* Click `Send`. There is a script that will save the authorization token received back in an `accessToken` variable in Postman.

> Client credentials token is valid for 30 minutes only. You can get a new token following the instructions above if you get a `401` error.

### Exercises

* [Create a Wishlist using Core and Custom Flows](core_custom_exercise/instructions.md)
* [Create Custom Gaming Attributes using Template Flows](template_exercise/instructions.md)
