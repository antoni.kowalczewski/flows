## 5. Create an Entry

In this exercise, you will create an entry with attribute values for your training gaming product.

### Create Entry

* Open the `Create an entry` request from `flows` folder.
* Replace the contents in the body section with:

```json
{
    "data": {
        "id": "{{productID}}",
        "type": "entry",
        "play_mode": "multiplayer",
        "age_rating": "T for Teen"
    }
}
```

* Click `Send`.
* The script in the Tests section of the request stores the Entry ID returned in a variable called `entryID`.

### Confirm template and entry have been applied

* Open the `Get a product by id` request from the `products` folder.
* Click `Send`. 
* The response will include an `extensions` section and look something like the following: 

```json
{
    "data": {
        "type": "product",
        "id": "69ed181a-9d72-4e6d-be86-c9b522032823",
        "attributes": {
            "commodity_type": "physical",
            "extensions": {
                "products(gaming)": {
                    "age_rating": "T for Teen",
                    "play_mode": "multiplayer"
                }
            },
            "name": "My PCM Training Product",
            "sku": "MPTP",
            "slug": "my-pcm-training-product",
            "status": "live"
        },...
    }
}
```


Congratulations! You have created a template with attributes specifically for your gaming products in the PCM API. You can apply this template to as many products as you need to, and add additional attributes as necessary. 