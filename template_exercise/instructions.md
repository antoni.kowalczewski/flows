## Create Custom Gaming Attributes using Template Flows

In this exercise, you will use template flows to extend the data schema to store additional attributes for your store's gaming products. You will then apply this template to a product and add a data entry. 

### Objectives

* Create gaming products. 
* Create a template for your custom gaming product attributes.
* Create specific attributes.
* Create product-template relationships.
* Create entries for gaming products.

### Exercises

1. [Create gaming products](./gaming_products.md)
2. [Create template Flow](./template_flow.md)
3. [Create attributes](./template_attributes.md)
4. [Create product-template relationship](./product_template_relationship.md)
5. [Create entries in a template Flow](./template-flow-entries.md)
