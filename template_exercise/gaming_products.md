## 1. Products

In this exercise, you will make a variety of CRUD calls to the Products endpoint. You will create a product, then create a hierarchy and a child node. You will attach your product to the node. 

>Note: If you have already created products, hierarchies, and nodes in the API hands on exercises, skip this step and move on to [2. Create Template Flow](./template_flow.md)

### Create a new product

* Open the `Create a product` request under `products` folder.
* Create a product with the following fields: 
    * name: My PCM Training Product
    * slug: my-pcm-training-product
    * sku: MPTP
    * description: My PCM Training Product
    * status: live
* Leave the other fields at their defaults.
* Click `Send`. The new product id is saved to a `productId` variable, and the product sku is saved to a `productSku` variable.
* Add a new variable called `productID` and copy and paste the same ID value here. The flows requests later will be looking for `productID` and not `productId`. 
* Verify the response.

### Get a product 

* Open the `Get a product by id` request under `products` folder and click `Send`. 
* Verify the response matches the product you created above. 

### Create a new hierarchy

* Open the `Create a hierarchy` request under `hierarchies` folder. 
* Create a brand with the following fields: 
    * name: My Training Hierarchy
    * slug: my-training-hierarchy
    * description: A hierarchy for training purposes
* Click `Send`. The new brand id is saved to a `hierarchyId` variable.
* Verify the response.

### Get a hierarchy

* Open the `Get a hierarchy by id` request under `hierarchies` folder and click `Send`. 
* Verify the response matches the hierarchy you created above.

### Create a new node

* Open the `Create a node` request under `nodes` folder to create a node directly under the hierarchy you just created. Note the url for this request uses the `hierarchyId` variable.
* Create a node with the following fields: 
    * name: My Training Node
    * slug: my-training-node
    * description: A node for training purposes
* Click `Send`. The new node id is saved to a `nodeId` variable.
* Verify the response.

### Get a node

* Open the `Get a node by id` request under `nodes` folder and click `Send`. 
* Verify the response matches the node you created above.

* Open the `Get a hierarchy's child nodes` request under `hierarchies` folder and click `Send`.
* Verify the response matches the node you created above.

### Create a product-node relationship

* Open the `Create relationships to products` request under `nodes` folder to attach your product to the node. Note the url for this request uses the `nodeId` variable.
* The body is an array of products to allow you to create relationships for multiple products to a single node at a time. However, you only have one product right now, so leave the default body as it is.
* Click `Send`. 
* Verify the response.

### Get a node's products

* Open the `Get a node's products` request under `nodes` folder and click `Send`. 
* Verify the response matches the product-node relationship you created above.

[Next: 2. Create Template Flow](./template_flow.md)
